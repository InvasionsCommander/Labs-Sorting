#include <stdio.h>	//в этом заголовочном файле определена функция printf
#include <stdlib.h>	//в этом заголовочном файле определена функция malloc
#include <string.h>	//в этом заголовочном файле определена функция memcpy

//блок функций общего назначения
void print_array(int A[], unsigned n);	//вывод массива на печать
void swap(int* pa, int* pb);			//обмен переменных значениями

//блок вспомогательных функций, реализующих сдвиг элементов массива
void right_shift(int A[], unsigned left_index, unsigned right_index);		//сдвиг вправо
void right_shift_cycle(int A[], unsigned left_index, unsigned right_index);	//циклический сдвиг вправо

//блок вспомогательныъ функций для поиска минимума и максимума
unsigned min_index(int A[], unsigned n);	//поиск индекса минимального элемента массива
unsigned max_index(int A[], unsigned n);	//поиск индекса максимального элемента массива

//блок функций, реализующих поиск в ОТСОРТИРОВАННОМ массиве
unsigned linear_search(int A[], unsigned n, int value);				//линейный поиск
unsigned binary_search(int A[], unsigned n, int value);				//бинарный поиск
unsigned binary_search_recursion(int A[], unsigned n, int value);	//бинарный поиск, реализованный с помощью рекурсии

//базовые методы сортировки
void bubble_sort(int A[], unsigned n);				//пузырьковая сортировка
void bubble_sort_recursion(int A[], unsigned n);	//пузырьковая сортировка, реализованный с помощью рекурсии
void select_sort(int A[], unsigned n);				//сортировка выбором
void select_sort_recursion(int A[], unsigned n);	//сортировка выбором, реализованный с помощью рекурсии
void insert_sort(int A[], unsigned n);				//сортировка вставками

//сортировка слиянием
void merge_list(int A[], unsigned length1, unsigned length2);		//реализация слияния на основе циклических сдвигов без использования буфера
void merge_list_buf(int A[], unsigned length1, unsigned length2);	//реализация слияния с использованием динамически выделяемого буфера
void merge_sort(int A[], unsigned n);								//рекурсивная процедура деления массива пополам с последующим слиянием

//быстрая сортировка
void split(int A[], unsigned n, int pivot, unsigned *pLeft, unsigned *pRight);		//реализация разделения массива на элементы меньше опорного и больше опорного
void split_buf(int A[], unsigned n, int pivot, unsigned *pLeft, unsigned *pRight);	//реализация разделения массива с использованием динамически выделяемого буфера
void quick_sort(int A[], unsigned n);												//рекурсивное разделение массива

//функции для рационального выбора осевого элемента в быстрой сортировке
int get_middle3(int a, int b, int c);			//получить медиану для 3 чисел
int get_middle2(int a, int b);					//получить медиану для 3 чисел
int get_pivot_avg(int A[], unsigned n);			//получить элемент массива, наиболее близкий к среднему арифметическому
int get_pivot_min_max(int A[], unsigned n);		//получить элемент массива, наиболее близкий к среднему между min и max
int get_pivot_median(int A[], unsigned n);		//получить оценку медианы

//функция сравнения целых чисел для демонстрации работы библиотечной qsort
int int_compare(const void* pa, const void* pb);

int main()
{
	const unsigned N = 20;
	int A[N] = { 45,-21,34,2,-54,99,37,45,83,-13,22,39,-69,13,17,7,11,19,23,-15 };

	printf("Before sort:\n");
	print_array(A, N);

	//bubble_sort (A,N);
	//bubble_sort_recursion(A, N);
	//select_sort(A, N);
	//select_sort_recursion(A, N);
	//insert_sort(A, N);
	//merge_sort(A, N);
	//quick_sort(A, N);
	
	//демонстрация применения библиотечной функции сортировки
	//для её реализации требуется написать функцию сравнения элементов массива
	//и передать указатель на неё в качестве последнего аргумента
	//посмотрите прототип функции - там везде речь идёт о void*
	//а при проведении самого сравнения нужно знать тип данных (в нашем случае int)
	//для этого используется указатель на функцию int_compare
	qsort(A, N, sizeof(int), int_compare);
	
	printf("After sort:\n");
	print_array(A, N);

	getchar();
    return 0;
}

void print_array(int A[], unsigned n)
{
	for (unsigned i = 0; i < n; i++)
		printf("%4d", A[i]);
	printf("\n");
}

void swap(int* pa, int* pb)
{
	int temp = *pa;
	*pa = *pb;
	*pb = temp;
	return;
}

void right_shift(int A[], unsigned left_index, unsigned right_index)
{
	for (unsigned i = right_index; i > left_index; i--)
		A[i] = A[i-1];
	return;
}

void right_shift_cycle(int A[], unsigned left_index, unsigned right_index)
{
	/*
	Циклический сдвиг
	Помимо собственно сдвига берёт последний элемент (с индексом right_index)
	и помещает его слева (в позицию с индексом left_index)
	*/
	int temp = A[right_index];
	right_shift(A, left_index, right_index);
	A[left_index] = temp;
}

unsigned min_index(int A[], unsigned n)
{
	unsigned index_min = 0;
	for (unsigned i = 1; i < n; i++)
		if (A[index_min] > A[i])
			index_min = i;
	return index_min;
}

unsigned max_index(int A[], unsigned n)
{
	unsigned index_max = 0;
	for (unsigned i = 1; i < n; i++)
		if (A[index_max] < A[i])
			index_max = i;
	return index_max;
}

unsigned linear_search(int A[], unsigned n, int value)
{
	/*
	Линейный поиск индекса элемента ОТСОРТИРОВАННОГО массива,
	перед которым следует вставить значение value,
	чтобы массив не потерял своей упорядоченности
	Реализация линейного поиска в данном случае учитывает,
	что в отсортированном массиве как минимум самый правый элемент больше искомого
	(в рамках алгоритма сортировки вставками это гарантируется)
	Если это не так, то такая реализация небезопасна - возможно,
	что функция не вернёт значение, т.к. return спрятан внутри условного оператора if
	*/
	for (unsigned i = 0; i < n; i++)
		if (A[i] > value)
			return i;
}

unsigned binary_search(int A[], unsigned n, int value)
{
	unsigned left = 0, right = n - 1, middle;
	
	/*
	Бинарный поиск индекса элемента массива,
	перед которым следует вставить значение value,
	чтобы массив не потерял своей упорядоченности
	Массив отсортирован, иначе бы бинарный поиск был бы просто не возможен
	*/

	do
	{
		//если элемент следует поставить перед левой границей рассматриваемого отрезка
		//можно сразу вернуть это значение
		if (A[left] > value)
			return left;

		middle = (left + right) / 2;
		//если попали в элемент с тем же значением - вставляем перед ним
		if (A[middle] == value)
			return middle;
		else
			//иначе двигаем правую или левую границу в центр
			if (A[middle] > value)
				right = middle;
			else
				left = middle;
	//продолжаем, пока рассматриваемый отрезок массива не сократится до 1 элемента
	} while ((right-left)>1);
	
	//в этом случае следует вставлять новый элемент перед его правой границей
	return right;
}

unsigned binary_search_recursion(int A[], unsigned n, int value)
{
	/*
	Бинарный поиск индекса элемента массива,
	перед которым следует вставить значение value,
	чтобы массив не потерял своей упорядоченности
	Массив отсортирован, иначе бы бинарный поиск был бы просто не возможен
	Реализация основана на рекурсивном принципе
	*/

	unsigned middle;

	//Если первый элемент массива больше value,
	//значит поиск можно завершить - место для вставки найдено
	//*A - аналог A[0]
	if (*A > value)
		return 0;
	//частный случай - для короткого массива
	if (n <= 2) return 1;
	middle = n / 2;
	//если попали в элемент с тем же значением - вставляем перед ним
	if (A[middle] == value)
		return middle;
	else
		if (A[middle] > value)
			//рекурсивный поиск в левой части массива
			//адрес первого элемента массива не меняется,
			//но в качестве длины выбираем middle
			return binary_search_recursion (A,middle,value);
		else
			//иначе - рекурсивный поиск в правой части массива
			//адрес первого элемента массива смещается на middle значений вправо,
			//длина такого массива - разность первоначальной длины и длины левой части
			return middle + binary_search_recursion(A+middle, n-middle, value);
}

void bubble_sort(int A[], unsigned n)
{
	/*
	Пузырьковая сортировка
	Предполагает многократное прохождение по массиву
	Если 2 соседних элемента не упорядочены - поменять их местами
	За один проход гарантируется, что самый большой элемент окажется в конце,
	поэтому для вложенного цикла условие продолжения -(j < (n-i-1))
	это позволяет избежать сравнений элементов,
	которые заведомо уже стоят в правильных позициях
	-1 необходима, иначе будет выход за пределы масива
	*/
	unsigned i, j, flag;
	//flag - признак упорядоченности массива

	for (i = 0; i < n-1; i++)
	{
		//перед очередным проходом по массиву выставляем flag в 1
		flag = 1;
		for (j = 0; j < (n-i-1); j++)
			if (A[j] > A[j + 1])
			{
				swap(A + j, A + j + 1);		//используем адресную арифметику
				//swap(&A[j], &A[j + 1]);	//аналог - "в лоб"
				//если произведён хотя бы 1 обмен, сбрасываем flag в 0
				flag = 0;
			}
		//print_array(A, n);	//отладочный вывод
		
		//если в ходе прохода по массиву не было обменов
		//сортировку можно счиать завершённой
		if (flag) break;
	}
	return;
}

void bubble_sort_recursion(int A[], unsigned n)
{
	/*
	Рекурсивная реализация пузырьковой сортировки
	*/

	unsigned i, flag = 1;
	//назначение flag - см. обычную пузырьковую сортировку

	//по сути, внутренний цикл из обычной пузырьковой сортировки
	for (i = 0; i < (n-1); i++)
		if (A[i] > A[i + 1])
		{
			swap(A + i, A + i + 1);
			flag = 0;
		}
	
	//Если массив всё ешё не отсортирован,
	//то сделать рекурсивный вызов
	//По результатам работы цикла выше самый большой элемент встал в конец массива
	//поэтому можем запустить задачу для того же массива,
	//но считая его длину меньше на 1
	if (!flag) bubble_sort_recursion(A,n-1);
	return;
}

void select_sort(int A[], unsigned n)
{
	/*
	Сортировка прямым выбором
	В массиве выделяются 2 части:
	уже отсортированная (слева) и ещё нет (справа)
	На каждом шаге выбирается минимальный элемент
	(вернее, его индекс) в неотсортированной части массива
	после этого он помещается в начало этой части
	При этом его можно перевести в число отсортированых элементов
	*/
	unsigned index_min;

	for (unsigned i = 0; i < n-1; i++)
	{
		//Индекс минимального элемента ищем в НЕОТСОРТИРОВАННОЙ части массива
		//поэтому к адресу добавляем смещение i
		//i можно трактовать как "число уже отсортированных элементов"
		index_min = min_index(A + i, n-i);
		//Ставим эту проверку, т.к. если минимальный элемент
		//стоит первым, менять его с самим собой нет смысла
		if (index_min > 0)
			//A+i - адрес начала неотсортированной части массива
			//и к нему ещё добавляем смещение index_min
			swap(A + i, A + i + index_min);
		//print_array(A, n);		//отладочный вывод
	}
	return;
}

void select_sort_recursion(int A[], unsigned n)
{
	/*
	Рекурсивная реализация сортировки прямым выбором
	*/

	unsigned index_min = min_index(A, n);
	
	//Ставим эту проверку, т.к. если минимальный элемент
	//стоит первым, менять его с самим собой нет смысла
	if (index_min > 0)
		swap(A, A + index_min);
	
	//рекурсия рано или поздно должна заканчиваться
	//поэтому рекурсивные вызовы делаем только в случае массивов с n>2
	if (n>2)
		//"забываем" про уже отсортированные элементы
		//(вернее, элемент)
		//вызываем сортировку для оставшейся части массива
		//поэтому адрес смещаем на один элемент вправо: A+1
		select_sort_recursion(A + 1, n - 1);
}

void insert_sort(int A[], unsigned n)
{
	/*
	Сортировка прямой вставкой
	Массив делится на 2 части:
	отсортированную (слева) и ещё нет (справа)
	*/
	unsigned position_to_insert;
	int temp;
	//Первый элемент массива изначально считаем относящимся к отсортированной части,
	//поэтому индексацию начинаем с i=1
	for (unsigned i = 1; i < n; i++)
	{
		//если очередной элемент меньше самого правого элемента
		//(самого большого) в отсортированной части массива
		if (A[i] < A[i - 1])
		{
			//ищем позицию для встравки одним из методов
			//по поводу аргументов функций:
			//ищем где: начиная с адреса A
			//сколько элементов просматриваем: i
			//ищем место для какого элемента: A[i]
			position_to_insert = linear_search(A, i, A[i]);					//ищем линейным поиском
			//position_to_insert = binary_search(A, i, A[i]);				//ищем бинарным поиском
			//position_to_insert = binary_search_recursion(A, i, A[i]);		//ищем бинарным поиском с рекурсивной реализацией
			
			//процедура вставки с использованием обычного сдвига вправо
			//при этом сохраняем значение A[i]
			//оно будет затёрто при сдвиге, но потом вставим его на позицию слева
			//temp = A[i];
			//right_shift(A, position_to_insert, i);
			//A[position_to_insert] = temp;
			
			//все эти действия вместе - при ЦИКЛИЧЕСКОМ сдвиге вправо
			right_shift_cycle(A, position_to_insert, i);
		}
		//print_array(A, n);
	}
	return;
}

void merge_list(int A[], unsigned length1, unsigned length2)
{
	/*
	Слияние двух отсортированных частей массива БЕЗ использования буфера
	Первая часть массива имеет длину length1, вторая - length2
	*/

	//i1 и i2 - индексы рассматриваемых элементов частей массива
	//массивы отсортированы, двигаемся слева направо
	//таким образом указывают на минимальные элементы в каждой из частей
	//(среди ещё не рассмотренных)
	unsigned i1=0, i2=length1;

	while (i1 < i2 && i2 < (length1+length2))
	{
		//если меньший элемент стоит в левой части массива
		if (A[i1] <= A[i2])
			//просто сдвигаем счётчик
			//элемент и так стоит на своём месте
			i1++;
		else
			//вынуждены сделать циклический сдвиг вправо
			//фактически, вставляем рассматриваемый элементы из второй части,
			//а все оставшиеся элементы первой части сдвигаем вправо
			//после сдвига оба индекса увеличиваем на 1
			right_shift_cycle(A, i1++, i2++);
	}
}

void merge_list_buf(int A[], unsigned length1, unsigned length2)
{

	/*
	Слияние двух отсортированных частей массива с использованием буферов
	Первая часть массива имеет длину length1, вторая - length2
	*/
	
	//создаём указатели для работы с буферами
	//память под буферы при этом НЕ выделяется!
	int* buf1;
	int* buf2;
	
	unsigned i1, i2;

	//выделяем память под буферы при помощи malloc,
	//инициализируем созданные указатели
	//память выделяется в байтах - поэтому не забываем про использование sizeof
	//malloc возвращает указатель на void ("void*"),
	//поэтому на забываем явно привести его к указателю на используемый тип данных
	//(в данном случае - "int*")
	buf1 = (int*)malloc(length1 * sizeof(int));
	buf2 = (int*)malloc(length2 * sizeof(int));
	//Выделили память при помощи malloc?
	//Не забываем тут же предусмотреть вызов free!!!


	//поэлементное копирование
	/*
	//копирование первой части массива в первый буфер
	for (i1 = 0; i1 < length1; i1++)
		buf1[i1] = A[i1];
	//копирование первой части массива в первый буфер
	for (i2 = 0; i2 < length2; i2++)
		buf2[i2] = A[length1+i2];
	*/

	//тоже копирование массивов, но уже целыми областями памяти
	memcpy(buf1, A, length1 * sizeof(int));
	memcpy(buf2, A + length1, length2 * sizeof(int));

	i1 = i2 = 0;

	//выбираем минимальные элементы из буферов, пока один из них не исчерпается
	while ((i1 < length1) && (i2 < length2))
	{
		if (buf1[i1] < buf2[i2])
		{
			A[i1 + i2] = buf1[i1];
			i1++;
		}
		else
		{
			A[i1 + i2] = buf2[i2];
			i2++;
		}
	}

	//выбираем всё, что осталось из первого буфера
	while (i1 < length1)
	{
		A[i1 + i2] = buf1[i1];
		i1++;
	}

	//выбираем всё, что осталось из второго буфера
	while (i2 < length2)
	{
		A[i1 + i2] = buf2[i2];
		i2++;
	}

	//не забываем чистить динамически распределённую память!!!
	free(buf1);
	free(buf2);
}

void merge_sort(int A[], unsigned n)
{
	/*
	Сортировка слиянием
	Алгоритм изначально имеет рекурсивный принцип построения
	Рассматриваемая часть массива делится пополам,
	для каждой из половин вызывается тот же алгоритм
	После их сортировки "сливаем" вместе 2 уже отсортированных подмассива
	*/

	//в реальной ситуации чаще используется несколько if
	//в данном случае switch использован в демонстрационных целях
	switch (n)
	{
	case 0:
	case 1:
		return;		//действие, которое произойдёт при n=0 или n=1 (когда сортировать по сути нечего)
		//break;	//в switch каждую альтернативную ветку следует завершать break
					//в данном случае это не сделано только потому,
					//что return не только выйдет из switch, но и завершит всю функцию
	case 2:
		if (A[0] > A[1])	//любой метод сортировки для массива из 2 элементов проведёт это сравнение
			swap(A, A + 1);
		return;
		//break;	//см. комментарий выше
	default:				//для всех массивов длиной > 2
		unsigned length1 = n / 2;			//здесь целочисленное деление, с отбрасываением остатка
		unsigned length2 = n - length1;		//корректно обработает нечётные n
	
		//рекурсивные вызовы
		merge_sort(A, length1);				//сортируем левую часть массива длиной lenght1
		merge_sort(A+length1, length2);		//сортируем правую часть массива
											//(начиная с адреса, смещённого на lenght1)
											//длиной lenght2
		
		//слияние
		//merge_list_buf(A, length1, length2);		//с использованием буферов
		merge_list(A, length1, length2);			//без использования буферов
	}
}

void split(int A[], unsigned n, int pivot, unsigned *pLeft, unsigned *pRight)
{
	/*
	Алгоритм разделения массива в ходе быстрой сортировки БЕЗ использованием буфера
	Гарантируется, что массив разделится на 2 части:
	- элементы меньше ИЛИ РАВНЫЕ pivot
	- элементы больше ИЛИ РАВНЫЕ pivot
	возможно, что между ними образуются элементы,
	равные pivot, но это ЧАСТНЫЙ случай
	Этот принцип деления принципиально отличает эту процедуру разделения
	от реализованной ниже процедуры разделения с использованием буфера,
	где гарантируется, что элементы равные pivot сразу попадают на свои позиции
	в упорядоченном массиве
	*/

	//здесь очень важно, что тип знаковый (int),
	//т.к. right может стать отрицательным
	//и это проверяется в конце алгоритма
	int left = 0, right = n - 1;
		
	//формируем подмассивы справа и слева
	//нестрогое неравенство - чтобы left и right гарантировано перехлестнулись
	while (left <= right)
	{
		while (A[left] < pivot)
			left++;
		
		while (A[right] > pivot)
			right--;
		
		//нестрогое неравенство - чтобы сдвинуть left и right, иначе зациклимся
		if (left <= right)
			swap(A + left++, A + right--);
	}

	//тут не ошибка с правой и левой стороой:
	//мы учитываем, что был перехлёст left и right
	*pLeft = (right>0) ? right : 0;
	*pRight = (left < n) ? left : n-1;
}

void split_buf(int A[], unsigned n, int pivot, unsigned *pLeft, unsigned *pRight)
{
	/*
	Алгоритм разделения массива в ходе быстрой сортировки с использованием буфера
	В данном случае выделяем 3 категории элементов:
	- элементы СТРОГО меньшие pivot
	- элементы равные pivot
	- элементы СТРОГО большие pivot
	Упорядочиваем массив таким образом,
	чтобы элементы равные осевому
	сразу встали на свои места.
	Меньшие элементы должны оказаться левее,
	а большие - правее.
	*/


	//создаём буфер размером, аналогичным массиву
	//тут же распределяем под него память с помощью malloc
	int* buf = (int*)malloc(n * sizeof(int));
	unsigned i, left=0, right=n-1;

	//копируем массив в буфер
	memcpy(buf, A, n * sizeof(int));
	
	//формируем подмассивы справа и слева
	//в данном случае используем 2 индекса:
	//left и right
	//при этом по буферу проходим только 1 раз
	//(в цикле for)
	for (i = 0; i < n; i++)
		//при нахождении элемента в буфере меньше осевого
		if (buf[i] < pivot)
			//размещаем его в массиве слева,
			//индекс left увеличиваем
			A[left++] = buf[i];
		else
			//при нахождении элемента в буфере больше осевого
			if (buf[i] > pivot)
				//размещаем его в массиве справа,
				//индекс right уменьшаем
				A[right--] = buf[i];
	
	//середину заполняем значениями pivot
	//делаем это в цикле, поскольку элементов,
	//равных осевому может быть несколько
	for (i = left; i <= right; i++)
		A[i] = pivot;

	//освобождаем память, выделенную под буфер
	free(buf);

	//с использованием указателей сообщаем функции
	//сортировки индексы граничных элементов подмассивов
	*pLeft = left-1;
	*pRight = right+1;
}

void quick_sort(int A[], unsigned n)
{
	/*
	Сортировка слиянием
	Алгоритм изначально имеет рекурсивный принцип построения
	На каждом шаге массив делится на 2 части,
	для каждой из которых вызывается тот же алгоритм
	После сортировки подмассивов никакой обработки не нужно,
	но требуется предварительное переупорядочивание
	Для этого выберается так называемый опорный или осевой (pivot) элемент
	В левую часть массива попадают элементы, меньшие или равне pivot,
	а в правую - большие или равные pivot
	*/

	//в реальной ситуации чаще используется несколько if
	//в данном случае switch использован в демонстрационных целях
	switch (n)
	{
	case 0:
	case 1:
		return;		//когда сортировать по сути нечего
		//break;	//см. аналогичный комментарий к merge_sort
	case 2:
		if (A[0] > A[1])	//любой метод сортировки для массива из 2 элементов проведёт это сравнение
			swap(A, A + 1);
		return;
		//break;	//см. аналогичный комментарий к merge_sort
	default:		//для всех массивов длиной > 2
		int pivot;
		unsigned left, right;	//индексы внутренних границ левой и правой частей массива

		//выбираем опорный элемент
		
		//простейшие методы - ориентируемся только по индексам
		pivot = A[0];			//первый элемент массива
		//pivot = A[n-1];		//последний элемент массива
		//pivot = A[n/2];		//средний элемент массива

		//более продвинутые методы
		//идея - оценить значение медианы
		//pivot = get_pivot_avg(A, n);			//элемент, наиболее близкий к среднему арифметическому всех элементов массива
		//pivot = get_pivot_min_max(A, n);		//элемент, наиболее близкий к среднему между min и max элементами массива
		//pivot = get_pivot_median(A, n);		//оценка медианы


		//процедура разделения массива по осевому элементу
		//split_buf(A,n,pivot,&left,&right);		//с использованием буфера
		split(A, n, pivot, &left, &right);		//без использования буфера

		//рекурсивные вызовы
		quick_sort(A, left+1);					//сортируем левую часть массива
		quick_sort(A + right, n - right);		//сортируем правую часть массива
	}
}

int get_middle3(int a, int b, int c)
{
	/*
	Находим медиану для тройки чисел
	*/
	int buf[3] = { a,b,c };		//делаем из 3 независимых чисем короткий массив
	bubble_sort(buf, 3);		//сортируем его любым методом, на такой размерности они равнозначны
	return buf[1];				//возвращаем средний элемент
}

int get_middle2(int a, int b)
{
	/*
	Находим медиану для пары чисел
	Строго математически, её не существует
	Поэтому берём либо первое, либо второе число
	Для того, чтобы внести элемент случайности,
	проверяем сумму этих чисел на чётность
	*/
	return (a + b) % 2 ? a : b;
}

int get_pivot_avg(int A[], unsigned n)
{
	/*
	Считаем среднее арифметическое элементов массива,
	а затем находим наиболее близкий к этому среднему элемент
	Его поиск очень похож на поиск min или max
	*/

	int sum = 0, avg, pivot;
	unsigned i;

	for (i = 0; i < n; i++)
		sum += A[i];

	avg = sum / n;

	pivot = A[0];

	for (i = 0; i < n; i++)
		if (abs(pivot - avg) > abs(A[i] - avg))
			pivot = A[i];
	return pivot;
}

int get_pivot_min_max(int A[], unsigned n)
{
	/*
	Считаем среднее арифметическое min и max элементов массива,
	а затем находим наиболее близкий к этому среднему элемент
	Его поиск очень похож на поиск min или max
	*/

	int min, max, avg, pivot;
	unsigned i;

	min = A[min_index(A, n)];
	max = A[max_index(A, n)];

	avg = (min + max) / 2;

	pivot = A[0];

	for (i = 0; i < n; i++)
		if (abs(pivot - avg) > abs(A[i] - avg))
			pivot = A[i];
	return pivot;
}

int get_pivot_median(int A[], unsigned n)
{
	/*
	Приближённое вычисление медианы массива
	Алгоритм рекурсивный для n>4, для меньших длин
	реализуются альтернативные ветки при помощи switch
	*/
	switch (n)
	{
	case 1:
		return A[0];		//единственный элемент - сам по себе медиана
	case 2:
		return get_middle2(A[0], A[1]);		//медиана из двух
	case 3:
		return get_middle3(A[0],A[1],A[2]);	//медиана из трёх
	case 4:
		//медиана из двух среди двух пар элементов
		return get_middle2(get_middle2(A[0], A[1]), get_middle2(A[2], A[3]));
	default:
		//в общем случае делим массив на 3 части и находим медианы в каждой из них

		//рациональное деление на 3 части - для этого учитываем величину остатка от деления
		//например, для 8 просто n/3 дало бы разбиение 2,2,4
		//а в данном случае - 3,3,2 - т.е. более равномерно
		unsigned n_3 = (n%3 > 1) ? (n/3+1) : (n/3);
		int median1 = get_pivot_median(A, n_3);
		int median2 = get_pivot_median(A + n_3, n_3);
		int median3 = get_pivot_median(A + 2*n_3, n-2*n_3);
		//находим медиану из трёх вычисленных медиан для подмассивов
		return get_middle3(median1,median2,median3);
	}
}

int int_compare(const void* pa, const void* pb)
{
	//обратите внимание на слово const при описании параметров функции
	//это означает, что значение, на которое указывает указатель, не может быть изменено

	int a = *(int*)pa;		//указатели на void предварительно явно приводим к int*
	int b = *(int*)pb;
	
	return a-b;
}